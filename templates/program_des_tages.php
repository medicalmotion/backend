﻿<?php /* Template Name: Daily Program */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Fetch the post
the_post();

// Show header
get_header();

// Check if is default container
$is_vc_content = preg_match( "/\[vc_row.*?\]/i", $post->post_content );

// Password protected page doesn't use vc container
if ( post_password_required() ) {
	$is_vc_content = false;
}

// Page title (show or hide)
$show_title = false == $is_vc_content && is_singular() && kalium()->acf->get_field( 'heading_title' );

// Container start
$container = array();

if ( $is_vc_content ) {
	$container[] = 'vc-container';
} else {
	$container[] = 'container';
	$container[] = 'default-margin';
	
	if ( ! is_shop_supported() || ! ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ) {
		$container[] = 'post-formatting';
	}
}
?>
<div class="<?php echo esc_attr( implode( ' ', $container ) ); ?>">
<?php


// Show page title
if ( false == defined( 'HEADING_TITLE_DISPLAYED' ) && apply_filters( 'kalium_page_title', $show_title ) ) {
	?>
	<h1 class="wp-page-title"><?php the_title(); ?></h1>
	<?php
} 



// ---------------------------------------
// Added by Kevin Henry
// ---------------------------------------
?>

<?php
	$current_day = get_day_of_prio(get_prio_of_day(0));
	$exercises_of_the_day = get_exercises_of_day_for_prio(get_prio_of_day(0), $current_day);
	$release_difficulty = get_difficulty_for_zone(get_zone_for_exercise($exercises_of_the_day[0]),"r");
	$mobility_difficulty = get_difficulty_for_zone(get_zone_for_exercise($exercises_of_the_day[1]),"m");
	$control_difficulty = get_difficulty_for_zone(get_zone_for_exercise($exercises_of_the_day[2]),"c");
?>

<div class="vc-container">
	<section class="vc_section vc_custom_1519893220842 vc_section-has-fill">
		<div class="vc-parent-row row-default vc_custom_1519849436306">
			<div class="vc_row wpb_row vc_row-fluid">
				<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<h1 style="text-align: center;">Deine Übungen des Tages - Tag <?php echo get_user_day()?></h1>
									<h2 style="text-align: center;"><?php echo get_name_of_prio(get_prio_of_day(0));?></h2>
									<p>&nbsp;</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Mobile Buttons -->
		<div class="vc-parent-row row-default">
			<div class="vc_row wpb_row vc_row-fluid mobile_version">
				<div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_btn3-container  video_button vc_btn3-center" id="video_m_1">
					<?php 
					if ($release_difficulty == 'l')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 1</button>';
					}
					if ($release_difficulty == 'm')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 1</button>';
					}
					if ($release_difficulty == 's')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 1</button>';
					}
					?>
				</div></div></div></div>
				<div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_btn3-container  video_button vc_btn3-center" id="video_m_2">
					<?php 
					if ($mobility_difficulty == 'l')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 2</button>';
					}
					if ($mobility_difficulty == 'm')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 2</button>';
					}
					if ($mobility_difficulty == 's')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 2</button>';
					}
					?>
				</div></div></div></div>
				<div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_btn3-container  video_button vc_btn3-center" id="video_m_3">
					<?php 
					if ($control_difficulty == 'l')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 3</button>';
					}
					if ($control_difficulty == 'm')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 3</button>';
					}
					if ($control_difficulty == 's')
					{
						echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 3</button>';
					}
					?>
				</div></div></div></div>
				<div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_btn3-container  video_button vc_btn3-center" id="video_m_text">
					<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-blue">Infos</button>
				</div></div></div></div>
			</div>
		</div>
		<!-- PC Display -->
		<div class="vc-parent-row row-default vc_custom_1519851205889">
			<div class="vc_row wpb_row vc_row-fluid tagesprogramm">
				<div class="video_column wpb_column vc_column_container vc_col-sm-10 vc_col-has-fill">
					<div class="vc_column-inner vc_custom_1519848822830">
						<div class="wpb_wrapper">
							<div class="wpb_video_widget wpb_content_element vc_clearfix  tagesprogram_video  vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-center" id="exercise_1"><div class="wpb_wrapper"><div class="wpb_video_wrapper"><div class="video-as-holder">
								<div class="image-placeholder" style="padding-bottom: 56.25%;"></div>
								<?php
									echo '<iframe src="https://player.vimeo.com/video/'.get_exercise_for_difficulty($exercises_of_the_day[0], $release_difficulty).'" width="560" height="315" frameborder="0" title="21.1 - Nackenprogramm Intro" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								?>
							</div></div></div></div>
							<div class="wpb_video_widget wpb_content_element vc_clearfix  tagesprogram_video  vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-center" id="exercise_2"><div class="wpb_wrapper"><div class="wpb_video_wrapper"><div class="video-as-holder">
								<div class="image-placeholder" style="padding-bottom: 56.25%;"></div>
								<?php
									echo '<iframe src="https://player.vimeo.com/video/'.get_exercise_for_difficulty($exercises_of_the_day[1], $mobility_difficulty).'" width="560" height="315" frameborder="0" title="21.1 - Nackenprogramm Intro" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								?>
							</div></div></div></div>
							<div class="wpb_video_widget wpb_content_element vc_clearfix  tagesprogram_video  vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-center" id="exercise_3"><div class="wpb_wrapper"><div class="wpb_video_wrapper"><div class="video-as-holder">
								<div class="image-placeholder" style="padding-bottom: 56.25%;"></div>
								<?php
									echo '<iframe src="https://player.vimeo.com/video/'.get_exercise_for_difficulty($exercises_of_the_day[2], $control_difficulty).'" width="560" height="315" frameborder="0" title="21.1 - Nackenprogramm Intro" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
								?>
							</div></div></div></div>
							<div id="scrollbox-770677" class="lab-scroll-box  exercise_text " data-height="1000">
								<div class="lab-scroll-box-content" style="max-height: 1000px;">	
									<p>&#8220;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&#8221;</p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&#8220;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&#8221;</p>
									<p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&#8220;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&#8221;</p>
									p>&nbsp;</p>
									<p>&nbsp;</p>
									<p>&#8220;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&#8221;</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- PC Buttons -->
				<div class="button_column no_mobile_version wpb_column vc_column_container vc_col-sm-2">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="vc_btn3-container  video_button vc_btn3-center" id="video_1">
								<?php 
								if ($release_difficulty == 'l')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 1</button>';
								}
								if ($release_difficulty == 'm')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 1</button>';
								}
								if ($release_difficulty == 's')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 1</button>';
								}
								?>
							</div>
							<div class="vc_btn3-container  video_button vc_btn3-center" id="video_2">
								<?php 
								if ($mobility_difficulty == 'l')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 2</button>';
								}
								if ($mobility_difficulty == 'm')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 2</button>';
								}
								if ($mobility_difficulty == 's')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 2</button>';
								}
								?>
							</div>
							<div class="vc_btn3-container  video_button vc_btn3-center" id="video_3">
								<?php 
								if ($control_difficulty == 'l')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-success">Übung 3</button>';
								}
								if ($control_difficulty == 'm')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-warning">Übung 3</button>';
								}
								if ($control_difficulty == 's')
								{
									echo '<button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-danger">Übung 3</button>';
								}
								?>
							</div>
							<div class="vc_btn3-container  video_button vc_btn3-center" id="video_text">
								<button class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-blue">Infos</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="vc-parent-row row-default">
			<div id="feedback_field" class="vc_row wpb_row vc_row-fluid">
				<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<p> </p>
									<h2 style="text-align: center;">Feedback</h2>
									<p style="text-align: center;">Gib uns dein Feedback so dass wir deine Übungen an dich anpassen können!</p>
								</div>
							</div>
							<div class="um-form">
								<form method="post" action="">
								<div class="um-row _um_row_1 " style="margin: 0 0 30px 0">
									<div class="um-col-1" style="display:flex; flex-direction:row;justify-content: space-between; flex-wrap: wrap">
										<div class="um-field um-field-release um-field-radio um-field-type_radio" data-key="release">
											<div class="um-field-label">
												<label>Wie anstregend fandest du die 1. Übung?</label>
												<div class="um-clear"></div>
											</div>
											<div class="um-field-area">
												<label class="um-field-radio">
													<input   type="radio" name="fb_release" value="1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu einfach</span>
												</label>
												<label class="um-field-radio active">
													<input   type="radio" name="fb_release" value="0" checked />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-on"></i>
													</span>
													<span class="um-field-radio-option">genau richtig</span>
												</label>
												<div class="um-clear"></div>
												<label class="um-field-radio">
													<input   type="radio" name="fb_release" value="-1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu anstrengend</span>
												</label>
												<div class="um-clear"></div>
											</div>
										</div>
										<div class="um-field um-field-mobility um-field-radio um-field-type_radio" data-key="mobility">
											<div class="um-field-label">
												<label>Wie anstregend fandest du die 2. Übung?</label>
												<div class="um-clear"></div>
											</div>
											<div class="um-field-area">
												<label class="um-field-radio">
													<input   type="radio" name="fb_mobility" value="1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu einfach</span>
												</label>
												<label class="um-field-radio active">
													<input   type="radio" name="fb_mobility" value="0" checked />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-on"></i>
													</span>
													<span class="um-field-radio-option">genau richtig</span>
												</label>
												<div class="um-clear"></div>
												<label class="um-field-radio">
													<input   type="radio" name="fb_mobility" value="-1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu anstrengend</span>
												</label>
												<div class="um-clear"></div>
											</div>
										</div>
										<div class="um-field um-field-control um-field-radio um-field-type_radio" data-key="control">
											<div class="um-field-label">
												<label>Wie anstregend fandest du die 3. Übung?</label>
												<div class="um-clear"></div>
											</div>
											<div class="um-field-area">
												<label class="um-field-radio">
													<input   type="radio" name="fb_control" value="1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu einfach</span>
												</label>
												<label class="um-field-radio active">
													<input   type="radio" name="fb_control" value="0" checked />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-on"></i>
													</span>
													<span class="um-field-radio-option">genau richtig</span>
												</label>
												<div class="um-clear"></div>
												<label class="um-field-radio">
													<input   type="radio" name="fb_control" value="-1"  />
													<span class="um-field-radio-state">
														<i class="um-icon-android-radio-button-off"></i>
													</span>
													<span class="um-field-radio-option">zu anstrengend</span>
												</label>
												<div class="um-clear"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="um-col-alt">
									<div><input type="submit"  value="Weiter" class="um-button vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-block vc_btn3-color-blue" id="um-submit-btn" /></div>
									<div class="um-clear"></div>
								</div>	
								</form>
							</div>
						</div>
					</div>
		<style type="text/css">
			.um-button
			{
				text-align: center !important;
			}

			.um-field
			{
				margin: auto;
			}

			.um-1434.um .um-tip:hover,
			.um-1434.um .um-field-radio.active:not(.um-field-radio-state-disabled) i,
			.um-1434.um .um-field-checkbox.active:not(.um-field-radio-state-disabled) i,
			.um-1434.um .um-member-name a:hover,
			.um-1434.um .um-member-more a:hover,
			.um-1434.um .um-member-less a:hover,
			.um-1434.um .um-members-pagi a:hover,
			.um-1434.um .um-cover-add:hover,
			.um-1434.um .um-profile-subnav a.active,
			.um-1434.um .um-item-meta a,
			.um-account-name a:hover,
			.um-account-nav a.current,
			.um-account-side li a.current span.um-account-icon,
			.um-account-side li a.current:hover span.um-account-icon,
			.um-dropdown li a:hover,
			i.um-active-color,
			span.um-active-color
			{
				color: #3ba1da;
			}

			.um-1434.um .um-field-group-head,
			.picker__box,
			.picker__nav--prev:hover,
			.picker__nav--next:hover,
			.um-1434.um .um-members-pagi span.current,
			.um-1434.um .um-members-pagi span.current:hover,
			.um-1434.um .um-profile-nav-item.active a,
			.um-1434.um .um-profile-nav-item.active a:hover,
			.upload,
			.um-modal-header,
			.um-modal-btn,
			.um-modal-btn.disabled,
			.um-modal-btn.disabled:hover,
			div.uimob800 .um-account-side li a.current,div.uimob800 .um-account-side li a.current:hover
			{
				background: #3ba1da;
			}



			.um-1434.um .um-field-group-head:hover,
			.picker__footer,
			.picker__header,
			.picker__day--infocus:hover,
			.picker__day--outfocus:hover,
			.picker__day--highlighted:hover,
			.picker--focused .picker__day--highlighted,
			.picker__list-item:hover,
			.picker__list-item--highlighted:hover,
			.picker--focused .picker__list-item--highlighted,
			.picker__list-item--selected,
			.picker__list-item--selected:hover,
			.picker--focused .picker__list-item--selected {
				background: #44b0ec;
			}

			.um-1434.um {
				margin-left: auto!important;
				margin-right: auto!important;
			}.um-1434.um input[type=submit]:disabled:hover {
				background: #3ba1da;
			}.um-1434.um input[type=submit].um-button,
			.um-1434.um input[type=submit].um-button:focus,
			.um-1434.um a.um-button,
			.um-1434.um a.um-button.um-disabled:hover,
			.um-1434.um a.um-button.um-disabled:focus,
			.um-1434.um a.um-button.um-disabled:active {
				background: #3ba1da;
			}.um-1434.um a.um-link {
				color: #3ba1da;
			}.um-1434.um input[type=submit].um-button:hover,
			.um-1434.um a.um-button:hover {
				background-color: #44b0ec;
			}.um-1434.um a.um-link:hover, .um-1434.um a.um-link-hvr:hover {
				color: #44b0ec;
			}.um-1434.um .um-button {
				color: #ffffff;
			}.um-1434.um .um-button.um-alt,
			.um-1434.um input[type=submit].um-button.um-alt {
				background: #eeeeee;
			}.um-1434.um .um-button.um-alt:hover,
			.um-1434.um input[type=submit].um-button.um-alt:hover{
				background: #e5e5e5;
			}.um-1434.um .um-button.um-alt,
			.um-1434.um input[type=submit].um-button.um-alt {
				color: #666666;
			}
			.um-1434.um .um-form input[type=text],
			.um-1434.um .um-form input[type=tel],
			.um-1434.um .um-form input[type=number],
			.um-1434.um .um-form input[type=password],
			.um-1434.um .um-form textarea,
			.um-1434.um .upload-progress,
			.select2-container .select2-choice,
			.select2-drop,
			.select2-container-multi .select2-choices,
			.select2-drop-active,
			.select2-drop.select2-drop-above
			{
				border: 2px solid #ddd !important;
			}

			.um-1434.um .um-form .select2-container-multi .select2-choices .select2-search-field input[type=text] {border: none !important}


			.um-1434.um .um-form input[type=text]:focus,
			.um-1434.um .um-form input[type=tel]:focus,
			.um-1434.um .um-form input[type=number]:focus,
			.um-1434.um .um-form input[type=password]:focus,
			.um-1434.um .um-form .um-datepicker.picker__input.picker__input--active,
			.um-1434.um .um-form .um-datepicker.picker__input.picker__input--target,
			.um-1434.um .um-form textarea:focus {
				border: 2px solid #bbb !important;
			}

			.um-1434.um .um-form input[type=text],
			.um-1434.um .um-form input[type=tel],
			.um-1434.um .um-form input[type=number],
			.um-1434.um .um-form input[type=password],
			.um-1434.um .um-form textarea,
			.select2-container .select2-choice,
			.select2-container-multi .select2-choices
			{
				background-color: #ffffff;
			}

			.um-1434.um .um-form input[type=text]:focus,
			.um-1434.um .um-form input[type=tel]:focus,
			.um-1434.um .um-form input[type=number]:focus,
			.um-1434.um .um-form input[type=password]:focus,
			.um-1434.um .um-form textarea:focus {
				background-color: #ffffff;
			}


			.um-1434.um .um-form ::-webkit-input-placeholder
			{
				color:  #aaaaaa;
				opacity: 1 !important;
			}

			.um-1434.um .um-form ::-moz-placeholder
			{
				color:  #aaaaaa;
				opacity: 1 !important;
			}

			.um-1434.um .um-form ::-moz-placeholder
			{
				color:  #aaaaaa;
				opacity: 1 !important;
			}

			.um-1434.um .um-form ::-ms-input-placeholder
			{
				color:  #aaaaaa;
				opacity: 1 !important;
			}

			.select2-default,
			.select2-default *,
			.select2-container-multi .select2-choices .select2-search-field input
			{
				color:  #aaaaaa;
			}


			.um-1434.um .um-field-icon i,
			.select2-container .select2-choice .select2-arrow:before,
			.select2-search:before,
			.select2-search-choice-close:before
			{
				color: #aaaaaa;
			}

			.um-1434.um span.um-req
			{
				color: #aaaaaa;
			}

			.um-1434.um .um-field-label {
				color: #555555;
			}


			.um-1434.um .um-form input[type=text],
			.um-1434.um .um-form input[type=tel],
			.um-1434.um .um-form input[type=password],
			.um-1434.um .um-form textarea
			{
				color: #666666;
			}

			.um-1434.um .um-form input:-webkit-autofill {
			    -webkit-box-shadow:0 0 0 50px white inset; /* Change the color to your own background color */
			    -webkit-text-fill-color: #666666;
			}

			.um-1434.um .um-form input:-webkit-autofill:focus {
			    -webkit-box-shadow: none,0 0 0 50px white inset;
			    -webkit-text-fill-color: #666666;
			}


			.um-1434.um .um-tip {
				color: #cccccc;
			}
		</style>
		<!-- ULTIMATE MEMBER FORM INLINE CSS BEGIN --><style type="text/css"></style><!-- ULTIMATE MEMBER FORM INLINE CSS END --></td>
		</tr>
		</tbody>
		</table>
		</div>
	</div>
	</div></div></div></div></div>
</section>
<div class="vc-parent-row row-default">
	<div class="vc_row wpb_row vc_row-fluid">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner ">
				<div class="wpb_wrapper">
					<div class="wpb_raw_code wpb_raw_js" >
						<div class="wpb_wrapper">
							<script src="https://player.vimeo.com/api/player.js"></script>

							<script type="text/javascript"> 
								var button_1 = document.getElementById("video_1");
								var button_2 = document.getElementById("video_2");
								var button_3 = document.getElementById("video_3");
								var button_4 = document.getElementById("video_text");
								var button_m_1 = document.getElementById("video_m_1");
								var button_m_2 = document.getElementById("video_m_2");
								var button_m_3 = document.getElementById("video_m_3");
								var button_m_4 = document.getElementById("video_m_text");
								var feedback = document.getElementById("feedback_field");
								var button_1_pressed = false;
								var button_2_pressed = false;
								var button_3_pressed = false;

								function display_feedback(){
								if ((button_2_pressed == true) && (button_3_pressed == true)){
								feedback.style.display = "block";
								}
								}

								player_1 = new Vimeo.Player(document.getElementById("exercise_1").getElementsByTagName("iframe")[0]);
								player_2 = new Vimeo.Player(document.getElementById("exercise_2").getElementsByTagName("iframe")[0]);
								player_3 = new Vimeo.Player(document.getElementById("exercise_3").getElementsByTagName("iframe")[0]);

								button_1.onclick = function() {
								document.getElementById("exercise_1").style.display = "block";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementsByClassName('exercise_text')[0].style.display = "none";
								player_1.play();
								player_2.pause();
								player_3.pause();
								button_1_pressed = true;
								display_feedback();
								}

								button_m_1.onclick = function() {
								document.getElementById("exercise_1").style.display = "block";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementsByClassName('exercise_text')[0].style.display = "none";
								player_1.play();
								player_2.pause();
								player_3.pause();
								button_1_pressed = true;
								display_feedback();
								}

								button_2.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "block";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementsByClassName('exercise_text')[0].style.display = "none";
								player_1.pause();
								player_2.play();
								player_3.pause();
								button_2_pressed = true;
								display_feedback();
								}

								button_m_2.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "block";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementById("exercise_text").style.display = "none";
								player_1.pause();
								player_2.play();
								player_3.pause();
								button_2_pressed = true;
								display_feedback();
								}

								button_3.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "block";
								document.getElementsByClassName('exercise_text')[0].style.display = "none";
								player_1.pause();
								player_2.pause();
								player_3.play();
								button_3_pressed = true;
								display_feedback();
								}

								button_m_3.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "block";
								document.getElementsByClassName('exercise_text')[0].style.display = "none";
								player_1.pause();
								player_2.pause();
								player_3.play();
								button_3_pressed = true;
								display_feedback();
								}

								button_4.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementsByClassName('exercise_text')[0].style.display = "block";
								player_1.pause();
								player_2.pause();
								player_3.pause();
								}

								button_m_4.onclick = function() {
								document.getElementById("exercise_1").style.display = "none";
								document.getElementById("exercise_2").style.display = "none";
								document.getElementById("exercise_3").style.display = "none";
								document.getElementsByClassName('exercise_text')[0].style.display = "block";
								player_1.pause();
								player_2.pause();
								player_3.pause();
								}
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div></div>
	</div>
	<style>#feedback_field{
	display: none;
	}

	.mobile_version{
	display: none !important;
	}

	.vc_btn3-container{
	   margin-bottom: 0px;
	}

	.vc_btn3-container.vc_btn3-center .vc_btn3-block.vc_btn3-size-md{
	  padding: 14px 8px;
	}

	.vc_column-inner
	{
	padding-top: 0px !important;
	}

	.wpb_content_element
	{
	margin-bottom: 0px !important;
	}

	.no_mobile_version{
	display: block !important;
	}

	@media all and (max-width: 767px)
	{
	   .mobile_version{
	   display: flex !important;;
	   height: 54px;
	   }
	   
	   .no_mobile_version{
	   display: none !important;
	   }
	}

	@media all and (min-width: 768px)
	{
	   .exercise_text{
	   height: 351.55px !important;
	   }

	   .lab-scroll-box-content.ps{
	   max-height: 351.55px !important;
	   }
	}

	@media all and (min-width: 991px)
	{
	   .exercise_text{
	   height: 454.683px !important;
	   }

	   .lab-scroll-box-content.ps{
	   max-height: 454.683px !important;
	   }
	}

	@media all and (min-width: 1200px)
	{
	   .exercise_text{
	   height: 548.433px !important;
	   }

	   .lab-scroll-box-content.ps{
	   max-height: 548.433px !important;
	   }
	}

	.video_column{
	background-color: white;
	}

	.wpb_wrapper .lab-scroll-box{
	margin-bottom: 0px;
	}

	.vc_column_container > .vc_column-inner{
	padding-left: 0px;
	padding-right: 0px;
	}

	.vc_row{
	display: flex;
	flex-direction: row;
	}

	.button_column .vc_column-inner{
	height:100%;
	}

	.button_column .wpb_wrapper {
	height:100%;
	}

	.button_column .vc_btn3-container {
	margin-bottom: 0px;
	height:25%;
	}

	.button_column button {
	height:100%;
	}

	.video_button {
	margin-left: 5px;
	}

	#video_1{
	padding-bottom: 5px;
	}

	#video_m_1{
	margin-left: 0px;
	}

	#video_2{
	padding-bottom: 5px;
	}

	#video_3{
	padding-bottom: 5px;
	}

	#exercise_2{
	display: none;
	}

	#exercise_3{
	display: none;
	}

	.exercise_text{
	display: none;
	}</style>
</div>	


<?php
// ---------------------------------------
// ---------------------------------------
// Page content		
the_content();		

// Container end
?>
</div>
<?php

// Show footer
get_footer();