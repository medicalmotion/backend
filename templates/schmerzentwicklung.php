<?php /* Template Name: Pain Recap */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Fetch the post
the_post();

// Show header
get_header();

// Check if is default container
$is_vc_content = preg_match( "/\[vc_row.*?\]/i", $post->post_content );

// Password protected page doesn't use vc container
if ( post_password_required() ) {
	$is_vc_content = false;
}

// Page title (show or hide)
$show_title = false == $is_vc_content && is_singular() && kalium()->acf->get_field( 'heading_title' );

// Container start
$container = array();

if ( $is_vc_content ) {
	$container[] = 'vc-container';
} else {
	$container[] = 'container';
	$container[] = 'default-margin';
	
	if ( ! is_shop_supported() || ! ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ) {
		$container[] = 'post-formatting';
	}
}
?>
<div class="<?php echo esc_attr( implode( ' ', $container ) ); ?>">
<?php


// Show page title
if ( false == defined( 'HEADING_TITLE_DISPLAYED' ) && apply_filters( 'kalium_page_title', $show_title ) ) {
	?>
	<h1 class="wp-page-title"><?php the_title(); ?></h1>
	<?php
} 



// ---------------------------------------
// Added by Kevin Henry
// ---------------------------------------
?>

<div class="vc-container">
<div class="vc-parent-row row-default"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<h1 style="text-align: center;">Deine Schmerzentwicklung</h1>
			<p style="text-align: center;">Hier kannst du deinen aktuellen Schmerzstand sowie deine Fortschritte entdecken!</p>

		</div>
	</div>

<?php
	$user_day = get_user_day();

	// Create the chart
	echo '<div class="vc_chart vc_line-chart wpb_content_element" data-vc-legend="1" data-vc-tooltips="1" data-vc-animation="easeinOutCubic" data-vc-type="line" data-vc-values="{&quot;labels&quot;:[';
	// Add the Day Axis	
	for ( $i = 0; $i<= $user_day+2; $i++ )
	{
		echo '&quot; Tag '.($i+1).'&quot;,';
	}
	// Finish the day axis and add the customer data
	echo '&quot; &quot;],&quot;datasets&quot;:[{&quot;label&quot;:&quot;Du&quot;,&quot;fillColor&quot;:&quot;rgba(84, 114, 210, 0.1)&quot;,&quot;strokeColor&quot;:&quot;#5472d2&quot;,&quot;pointColor&quot;:&quot;#5472d2&quot;,&quot;pointStrokeColor&quot;:&quot;#5472d2&quot;,&quot;highlightFill&quot;:&quot;#3c5ecc&quot;,&quot;highlightStroke&quot;:&quot;#3c5ecc&quot;,&quot;pointHighlightFill&quot;:&quot;#3c5ecc&quot;,&quot;pointHighlightStroke&quot;:&quot;#3c5ecc&quot;,&quot;data&quot;:[';

	// Add the customer data
	$daily_difficulties = get_evolution_data_as_list(get_current_user_id());
	for( $i = 0; $i<count($daily_difficulties); $i++ ) 
	{
		if ($i < count($daily_difficulties)-1)
		{
			echo '&quot;'.$daily_difficulties[$i].'&quot;,';
		}
		else
		{
			echo '&quot;'.$daily_difficulties[$i].'&quot;';
		}
	}
	echo ']},{&quot;label&quot;:&quot;Durschnitt&quot;,&quot;fillColor&quot;:&quot;rgba(254, 108, 97, 0.1)&quot;,&quot;strokeColor&quot;:&quot;#fe6c61&quot;,&quot;pointColor&quot;:&quot;#fe6c61&quot;,&quot;pointStrokeColor&quot;:&quot;#fe6c61&quot;,&quot;highlightFill&quot;:&quot;#fe5043&quot;,&quot;highlightStroke&quot;:&quot;#fe5043&quot;,&quot;pointHighlightFill&quot;:&quot;#fe5043&quot;,&quot;pointHighlightStroke&quot;:&quot;#fe5043&quot;,&quot;data&quot;:[';

	// Add other customers statistics
	$statistics = get_evolution_of_customer_group(0, $user_day+2);
	for( $i = 0; $i<count($statistics); $i++ ) 
	{
		if ($i < count($statistics)-1)
		{
			echo '&quot;'.$statistics[$i].'&quot;,';
		}
		else
		{
			echo '&quot;'.$statistics[$i].'&quot;';
		}
	}
	echo ']}]}">';
?>
	<div class="wpb_wrapper">
		<div class="vc_chart-with-legend"><canvas class="vc_line-chart-canvas" width="1" height="1"></canvas></div>
	</div>
</div>
</div></div></div></div></div>
</div>

<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.4.5'></script>
<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.4.5'></script>
<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/lib/bower/chartjs/Chart.min.js?ver=5.4.5'></script>
<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/lib/vc_line_chart/vc_line_chart.min.js?ver=5.4.5'></script>	

<div class="vc-parent-row row-default">
	<div class="vc_row wpb_row vc_row-fluid">
		<h1 style="text-align: center;">Dein Program</h1>
		<p style="text-align: center;">Dein aktuelles Programm ist gerade so aufgebaut :</p>
		<?php
			echo '<div class="vc_chart vc_round-chart wpb_content_element" data-vc-legend="1" data-vc-tooltips="1" data-vc-animation="easeinOutCubic" data-vc-stroke-color="#ffffff" data-vc-stroke-width="2" data-vc-type="pie" data-vc-values="[';
			//First part of the chart :
			$value = 50;
			echo '{&quot;value&quot;:'.$value.',&quot;color&quot;:&quot;#5472d2&quot;,&quot;highlight&quot;:&quot;#3c5ecc&quot;,&quot;label&quot;:&quot;Fuss&quot;}';
			$value = 30;
			echo ',{&quot;value&quot;:'.$value.',&quot;color&quot;:&quot;#fe6c61&quot;,&quot;highlight&quot;:&quot;#fe5043&quot;,&quot;label&quot;:&quot;Ruecken&quot;}';
			$value = 20;
			echo ',{&quot;value&quot;:'.$value.',&quot;color&quot;:&quot;blue&quot;,&quot;highlight&quot;:&quot;blue&quot;,&quot;label&quot;:&quot;Nacken&quot;}';
			echo ']">';
		?>
			<div class="wpb_wrapper">
				<div class="vc_chart-with-legend">
					<canvas class="vc_round-chart-canvas" width="1" height="1"></canvas></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/lib/vc_chart/jquery.vc_chart.min.js?ver=5.4.5'></script>
<script type='text/javascript' src='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/lib/vc_round_chart/vc_round_chart.min.js?ver=5.4.5'></script>

<?php
// ---------------------------------------
// ---------------------------------------
// Page content		
the_content();		

// Container end
?>
</div>
<?php

// Show footer
get_footer();