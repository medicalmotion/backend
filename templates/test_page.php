<?php /* Template Name: Test page */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Fetch the post
the_post();

// Show header
get_header();

// Check if is default container
$is_vc_content = preg_match( "/\[vc_row.*?\]/i", $post->post_content );

// Password protected page doesn't use vc container
if ( post_password_required() ) {
	$is_vc_content = false;
}

// Page title (show or hide)
$show_title = false == $is_vc_content && is_singular() && kalium()->acf->get_field( 'heading_title' );

// Container start
$container = array();

if ( $is_vc_content ) {
	$container[] = 'vc-container';
} else {
	$container[] = 'container';
	$container[] = 'default-margin';
	
	if ( ! is_shop_supported() || ! ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ) {
		$container[] = 'post-formatting';
	}
}
?>
<div class="<?php echo esc_attr( implode( ' ', $container ) ); ?>">
<?php


// Show page title
if ( false == defined( 'HEADING_TITLE_DISPLAYED' ) && apply_filters( 'kalium_page_title', $show_title ) ) {
	?>
	<h1 class="wp-page-title"><?php the_title(); ?></h1>
	<?php
} 



// ---------------------------------------
// Added by Kevin Henry
// ---------------------------------------
echo get_user_day();
print_r(get_portfolio());
print_r(get_user_prio());
echo "<p>    Prio of the day :";
echo get_prio_of_day(0);
echo "</p><p>     Day of prio :";
echo get_day_of_prio(1);
echo "</p><p>    Day of prio :";
echo get_day_of_prio(2);
echo "</p><p>    Day of prio :";
echo get_day_of_prio(3);
echo "</p><p>    Program of prio :";
print_r(get_program_for_prio(1));
echo " </p><p>   Program of prio :";
print_r(get_program_for_prio(2));
echo " </p><p>   Program of prio :";
print_r(get_program_for_prio(3));
echo " </p><p>   Excercise of day for prio :";
echo get_exercises_of_day_for_prio(1,2);
echo " </p><p>   Zone of exercise :";
echo get_zone_for_exercise(1);
echo "  </p><p>  Zone of exercise :";
echo get_zone_for_exercise(2);
echo "</p><p>    Zone of exercise :";
echo get_zone_for_exercise(3);
echo "</p><p>    Difficulty of zone :";
echo get_difficulty_for_zone(1, "r");
echo "</p><p>    Difficulty of zone :";
echo get_difficulty_for_zone(2, "m");
echo "</p><p>    Difficulty of zone :";
echo get_difficulty_for_zone(3, "c");
echo "</p><p>    Exercise for difficulty :";
echo get_exercise_for_difficulty(3, "m");
echo "</p><p>    Exercise for difficulty :";
echo get_exercise_for_difficulty(201, "l");
echo "</p>";

// ---------------------------------------
// ---------------------------------------
// Page content		
the_content();		

// Container end
?>
</div>
<?php

// Show footer
get_footer();