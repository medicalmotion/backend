<?php
/**
 * Plugin Name: Own Plugins
 * Description: This plugin adds new functionalities.
 * Version: 1.0.0
 * License: GPL2
*/


// SLACK APP
function ar_post_to_slack($message, $channel, $username, $icon_emoji) {
	
	// Slack webhook endpoint from Slack settings
	$slack_endpoint = "https://hooks.slack.com/services/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";
	
	// Prepare the data / payload to be posted to Slack
	$data = array(
		'payload'   => json_encode( array(
			"channel"       =>  $channel,
			"text"          =>  $message,
			"username"	=>  $username,
			"icon_emoji"    =>  $icon_emoji
			)
		)
	);
	// Post our data via the slack webhook endpoint using wp_remote_post
	$posting_to_slack = wp_remote_post( $slack_endpoint, array(
		'method' => 'POST',
		'timeout' => 30,
		'redirection' => 5,
		'httpversion' => '1.0',
		'blocking' => true,
		'headers' => array(),
		'body' => $data,
		'cookies' => array()
		)
	);
}

function ar_user_register($user_id) {

	// get the current site url 
	$site_url = get_bloginfo('url');
	
	// prepare a message to be posted to slack 
	$message = 'Ein neuer Benutzer hat sich bei '.$site_url.' registriert ';
	
	// post the message to slack in the general channel 
	ar_post_to_slack($message,'#infos_webseite','Registrierung',':monkey:');
}

add_action('user_register', 'ar_user_register', 99, 2);


// FUNCTIONS

function connect_to_db() {
	$conn = mysqli_connect("mysql5.medicalmotion.de","db86729_80","Videre87K+","db86729_80");
	if(!$conn) {
		die('Problem in database connection: ' . mysql_error());
	}
	return $conn;
}

function get_portfolio(){
	$conn = connect_to_db();
	$sql = "SELECT `id`,`name` FROM `programs`";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
        	$array[$row['id']] = $row['name'];
    	}
    	return $array;
	}
	else {
		return 0;
	}
}

function get_user_day() {
	$id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `day` FROM `users` WHERE `id` = '$id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$day = $result->fetch_assoc()["day"];
	}
	return $day;
}

function set_user_day($day) {
	$id = get_current_user_id();
	$conn = connect_to_db();
	$query = "UPDATE `users` SET `day` = ('$day') WHERE `id` = '$id'";
	mysqli_query($conn, $query);
	$conn->close();
}

function get_user_prio(){
	$id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `goal_1`,`goal_2`,`goal_3` FROM `users` WHERE `id` = '$id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$user_data = $result->fetch_assoc();	
	}
	else
	{
		$user_data = 0;
	}
	return $user_data;
}

function get_prio_of_day($day_offset){
	// Day 1 : Prio 1
	// Day 2 : Prio 1
	// Day 3 : Prio 2
	// Day 4 : Prio 3
	// Day 5 : Prop 1
	// Day 6 : Prio 1
	// Day 7 : Prio 2
	$current_day = get_user_day()+$day_offset;
	if ($current_day % 7 == 1)
	{
		return get_user_prio()['goal_1'];
	}
	if ($current_day % 7 == 2)
	{
		return get_user_prio()['goal_1'];
	}
	if ($current_day % 7 == 3)
	{
		return get_user_prio()['goal_2'];
	}
	if ($current_day % 7 == 4)
	{
		return get_user_prio()['goal_3'];
	}
	if ($current_day % 7 == 5)
	{
		return get_user_prio()['goal_1'];
	}
	if ($current_day % 7 == 6)
	{
		return get_user_prio()['goal_1'];
	}
	if ($current_day % 7 == 0)
	{
		return get_user_prio()['goal_2'];
	}
}

function get_name_of_prio($prio_id){
	$conn = connect_to_db();
	$sql = "SELECT `name` FROM `programs` WHERE `id` = '$prio_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$prio_name = $result->fetch_assoc();
		return $prio_name['name'];
	}
	else 
	{
		return 0;
	}
}

function get_image_of_prio($prio_id){
	$conn = connect_to_db();
	$sql = "SELECT `image` FROM `programs` WHERE `id` = '$prio_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$prio_name = $result->fetch_assoc();
		$conn->close();
		return $prio_name['image'];
	}
	else 
	{
		return 0;
	}
}

function get_day_of_prio($prio_id)
{
	// Return the current day of a defined priority
	$id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `day_programs` FROM `users` WHERE `id` = '$id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$user_data = $result->fetch_assoc();
		// Get the prio days as an array of prio day
		$program_days = explode(",", substr($user_data["day_programs"],1,-1));
		// Get the prio of the program
		for ($i=0; $i<count($program_days); $i++)
		{
			$program_day = explode(":", $program_days[$i]);
			if ($program_day[0] == $prio_id)
			{
				return intval($program_day[1]);
			}
		}
	}
}

function set_day_of_prio($prio_id, $day)
{
	// Return the current day of a defined priority
	$id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `day_programs` FROM `users` WHERE `id` = '$id'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$user_data = $result->fetch_assoc();
		// Get the prio days as an array of prio day
		$program_days = explode(",", substr($user_data["day_programs"],1,-1));
		$new_day_programs = "{";
		// Get the prio of the program
		for ($i=0; $i<count($program_days); $i++)
		{
			$program_day = explode(":", $program_days[$i]);
			if ($program_day[0] == $prio_id)
			{
				$program_day[1] = $day;
			}
			$new_day_programs .= $program_day[0].':'.$program_day[1].',';
		}
		$new_day_programs = substr($new_day_programs,0,-1).'}';

		$query = "UPDATE `users` SET `day_programs` = ('$new_day_programs') WHERE `id` = '$id'";
		mysqli_query($conn, $query);
		$conn->close();
	}	
}

function get_program_for_prio($prio_id)
{
	// Return the program of a defined priority as an array of days
	$conn = connect_to_db();
	$sql = "SELECT `content` FROM `programs` WHERE `id` = '$prio_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$program_content = $result->fetch_assoc();
		// Get the prio days as an array of prio day
		$program_days = explode(";", $program_content["content"]);
	}
	return $program_days;

}

function get_exercises_of_day_for_prio($prio_id, $day_id)
{
	return explode(",", get_program_for_prio($prio_id)[$day_id-1]);
}

function get_zone_for_exercise($ex_id)
{
	// Return the zone of an exercise
	$conn = connect_to_db();
	$sql = "SELECT `zone` FROM `exercises` WHERE `id` = '$ex_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$ex_content = $result->fetch_assoc();
	}
	return $ex_content['zone'];
}

function get_name_of_zone($zone_id)
{
	// Defined statically
	if ($zone_id == 1)
	{
		return "Fuss";
	}
	if ($zone_id == 2)
	{
		return "Po Bereich";
	}
	if ($zone_id == 3)
	{
		return "Untere Rücken";
	}
	if ($zone_id == 4)
	{
		return "Shoulter/Nacken";
	}
}

function get_difficulty_for_zone($zone_id, $type)
{
	// Return the difficulty of a user for a zone
	$user_id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `difficulties` FROM `users` WHERE `id` = '$user_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$difficulties = $result->fetch_assoc();
		$difficulties = explode(";", substr($difficulties['difficulties'],1,-1));
		for ($i=0; $i<count($difficulties); $i++)
		{
			$zone_difficulty = explode(":", $difficulties[$i]);
			if ($zone_difficulty[0] == $zone_id)
			{
				if ($type == "r")
				{
					$difficulty = explode(",", substr($zone_difficulty[1],1,-1))[0];
				}
				if ($type == "m")
				{
					$difficulty = explode(",", substr($zone_difficulty[1],1,-1))[1];
				}
				if ($type == "c")
				{
					$difficulty = explode(",", substr($zone_difficulty[1],1,-1))[2];
				}
			}
		}
	}
	return $difficulty;
}

function set_difficulty_for_zone($zone_id, $type, $difficulty)
{
	$user_id = get_current_user_id();
	$conn = connect_to_db();
	$sql = "SELECT `difficulties` FROM `users` WHERE `id` = '$user_id'";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$difficulties = $result->fetch_assoc();
		$difficulties = explode(";", substr($difficulties['difficulties'],1,-1));
		$new_difficulties = "{";
		for ($i=0; $i<count($difficulties); $i++)
		{
			$zone_difficulty = explode(":", $difficulties[$i]);
			if ($zone_difficulty[0] == $zone_id)
			{
				$zone_difficulties = explode(",", substr($zone_difficulty[1],1,-1));
				if ($type == "r")
				{
					$zone_difficulty[1] = "[".$difficulty.",".$zone_difficulties[1].",".$zone_difficulties[2]."]";
				}
				if ($type == "m")
				{
					$zone_difficulty[1] = "[".$zone_difficulties[0].",".$difficulty.",".$zone_difficulties[2]."]";
				}
				if ($type == "c")
				{
					$zone_difficulty[1] = "[".$zone_difficulties[0].",".$zone_difficulties[1].",".$difficulty."]";
				}
			}
			$new_difficulties .= $zone_difficulty[0].":".$zone_difficulty[1].";";
		}
		$new_difficulties = substr($new_difficulties,0,-1).'}';
		$query = "UPDATE `users` SET `difficulties` = ('$new_difficulties') WHERE `id` = '$user_id'";
		mysqli_query($conn, $query);
		$conn->close();
	}
}

function get_exercise_for_difficulty($ex_id, $difficulty)
{
	$conn = connect_to_db();
	$sql = "SELECT `l_video`,`m_video`,`s_video` FROM `exercises` WHERE `id` = '$ex_id'";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows > 0) {
		$exercise = $result->fetch_assoc();
		if ($difficulty == "l")
		{
			return $exercise['l_video'];
		}
		if ($difficulty == "m")
		{
			return $exercise['m_video'];
		}
		if ($difficulty == "s")
		{
			return $exercise['s_video'];
		}
	}
	else
	{
		return "";
	}
}

function get_date($offset){
	$date = date("l d. M", mktime(0, 0, 0, date("m")  , date("d")+$offset, date("Y")));

	// Translate date
	$date = str_replace("Monday", "Montag", $date);
	$date = str_replace("Tuesday", "Dienstag", $date);
	$date = str_replace("Wednesday", "Mittwoch", $date);
	$date = str_replace("Thursday", "Donnerstag", $date);
	$date = str_replace("Friday", "Freitag", $date);
	$date = str_replace("Saturday", "Samstag", $date);
	$date = str_replace("Sunday", "Sonntag", $date);
	// Month
	$date = str_replace("Jan", "Januar", $date);
	$date = str_replace("Feb", "Februar", $date);
	$date = str_replace("Mar", "März", $date);
	$date = str_replace("Apr", "April", $date);
	$date = str_replace("May", "Mai", $date);
	$date = str_replace("Jun", "Juni", $date);
	$date = str_replace("Jul", "Juli", $date);
	$date = str_replace("Aug", "August", $date);
	$date = str_replace("Sep", "September", $date);
	$date = str_replace("Oct", "Oktober", $date);
	$date = str_replace("Nov", "November", $date);
	$date = str_replace("Dec", "Dezember", $date);
	return $date;
}

add_action( 'user_register', 'create_user_in_users_db');

function create_user_in_users_db($user_id) {
	$conn = connect_to_db();
	// Write default difficulties
	$release = "mittel";
	$mobility = "mittel";
	$control = "mittel";
	// Write default programm
	$program = "1,2,3;1,2,3;1,2,3;1,2,3;1,2,3";
	// Write default evolution for Day 1
	$evolution = "mittel,mittel,mittel";
	$query = "INSERT INTO `users` (`ID`, `release`, `mobility`, `control`, `program`, `evolution`) VALUES ('$user_id','$release','$mobility','$control','$program','$evolution')";
	mysqli_query($conn, $query);
	$conn->close();
}

add_action( 'init', 'write_feedback_user_db' );

function write_feedback_user_db() {
	if ((isset($_POST['fb_release'])) and (isset($_POST['fb_mobility'])) and (isset($_POST['fb_control']))) {

		$current_day = get_day_of_prio(get_prio_of_day(0));
		$exercises_of_the_day = get_exercises_of_day_for_prio(get_prio_of_day(0), $current_day);

		$release_ex = $exercises_of_the_day[0];
		$release_zone = get_zone_for_exercise($release_ex);
		$release_difficulty = get_difficulty_for_zone($release_zone, "r");

		if ($_POST['fb_release'] == -1)
		{
			if ($release_difficulty == 'm')
			{
				$release = 'l';
			}
			if ($release_difficulty == 's')
			{
				$release = 'm';
			}
		}
		if ($_POST['fb_release'] == 0)
		{
			$release = $release_difficulty;
		}
		if ($_POST['fb_release'] == 1)
		{
			if ($release_difficulty == 'l')
			{
				$release = 'm';
			}
			if ($release_difficulty == 'm')
			{
				$release = 's';
			} 
		}

		$mobility_ex = $exercises_of_the_day[1];
		$mobility_zone = get_zone_for_exercise($mobility_ex);
		$mobility_difficulty = get_difficulty_for_zone($mobility_zone, "m");

		if ($_POST['fb_mobility'] == -1)
		{
			if ($mobility_difficulty == 'm')
			{
				$mobility = 'l';
			}
			if ($mobility_difficulty == 's')
			{
				$mobility = 'm';
			}
			 
		}
		if ($_POST['fb_mobility'] == 0)
		{
			$mobility = $mobility_difficulty;
		}
		if ($_POST['fb_mobility'] == 1)
		{
			if ($mobility_difficulty == 'l')
			{
				$mobility = 'm';
			}
			if ($mobility_difficulty == 'm')
			{
				$mobility = 's';
			}	 
		}

		$control_ex = $exercises_of_the_day[2];
		$control_zone = get_zone_for_exercise($control_ex);
		$control_difficulty = get_difficulty_for_zone($control_zone, "c");

		if ($_POST['fb_control'] == -1)
		{
			if ($control_difficulty == 'm')
			{
				$control = 'l';
			}
			if ($control_difficulty == 's')
			{
				$control = 'm';
			} 
		}
		if ($_POST['fb_control'] == 0)
		{
			$control = $control_difficulty;
		}
		if ($_POST['fb_control'] == 1)
		{
			if ($control_difficulty == 'l')
			{
				$control = 'm';
			}
			if ($control_difficulty == 'm')
			{
				$control = 's';
			} 
		}

		echo "NEW_RELEASE :";
		echo $release;
		echo "NEW_MOBILITY :";
		echo $mobility;
		echo "NEW_CONTROL :";
		echo $control;

		// Update the zone difficulties
		set_difficulty_for_zone($release_zone, "r", $release);
		set_difficulty_for_zone($mobility_zone, "m", $mobility);
		set_difficulty_for_zone($control_zone, "c", $control);

		// Update the prio day
		set_day_of_prio(get_prio_of_day(0), $current_day+1);

		// Update the global day
		set_user_day(get_user_day() + 1);

	
		//header("Location: http://www.medicalmotion.de/programm-des-tages");
	} // end if
}

function add_data_to_evolution($user_id, $r_level, $m_level, $c_level) {
	$conn = connect_to_db();
	$sql = "SELECT `evolution` FROM `users` WHERE `id` = ('$user_id')";
	$result = $conn->query($sql);
	if ($result->num_rows == 1) {
    	$result = $result->fetch_assoc();
    	$evolution = $result['evolution'].";".$r_level.",".$m_level.",".$c_level;
    	$query = "UPDATE `users` SET `evolution` = ('$evolution') WHERE `id` = ('$user_id')";
	mysqli_query($conn, $query);	
    }
    $conn->close();	
}

function get_evolution_data($user_id) {
	$conn = connect_to_db();
	$sql = "SELECT `evolution` FROM `users` WHERE `id` = ('$user_id')";
	$result = $conn->query($sql);
	$conn->close();
	if ($result->num_rows == 1) {
    	$evolution = $result->fetch_assoc()['evolution'];

    }
    else {
    	$evolution = None;
    }
    return $evolution;
}

function get_evolution_data_as_list($user_id){
	$evolution_data = [];
	$daily_difficulties = get_evolution_data($user_id);
	$daily_difficulties_array = explode(";", $daily_difficulties);
	for( $i = 0; $i<count($daily_difficulties_array); $i++ ) 
	{
		$day_value = 0;
		$day_array = explode(",", $daily_difficulties_array[$i]);
		for ( $j = 0; $j<count($day_array); $j++ )
		{
			if ($day_array[$j] == 'leicht')
			{
				$day_value += 3;
			}
			if ($day_array[$j] == 'mittel')
			{
				$day_value += 2;
			}
			if ($day_array[$j] == 'schwer')
			{
				$day_value += 1;
			}
		}
		array_push($evolution_data, $day_value);
	}
	return $evolution_data;
}

function get_evolution_of_customer_group($group_id, $nb_days)
{
	// Check in the database for customer of the same group 
	// For now just dummy datas
	$random_data = [7,7,7,7,7,6,6,6,5,5,4,4,4,4,4,4,3,3,3,2,2,1,1];
	return array_slice($random_data, 0, $nb_days);
}

add_action( 'init', 'define_new_prio_event' );

function define_new_prio_event() {
	if ((isset($_POST['goal_1'])) and (isset($_POST['goal_2'])) and (isset($_POST['goal_3']))) {
		define_new_prio($_POST['goal_1'], $_POST['goal_2'], $_POST['goal_3']);
		header("Location: http://www.medicalmotion.de/dein-dashboard-template/");
	}
}

function define_new_prio($goal_1, $goal_2, $goal_3) {
	$id = intval(get_current_user_id());
	$conn = connect_to_db();
	$query = "UPDATE `users` SET `goal_1` = ('$goal_1'), `goal_2` = ('$goal_2'), `goal_3` = ('$goal_3') WHERE `id` = '$id'";
	mysqli_query($conn, $query);
	$conn->close();
}