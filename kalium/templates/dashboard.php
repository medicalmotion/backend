<?php /* Template Name: Dashboard */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Direct access not allowed.
}

// Added by Kevin Henry
// Get which programm to do, and if the customer is changing his program, start from day 1 again
if (isset($_GET['id']))
{
	if (filter_var($_GET['id'], FILTER_VALIDATE_INT) == True)
	{
		define_program($_GET['id']);
	}
    else
    {
    	define_program(0);
    }
}


// Fetch the post
the_post();

// Show header
get_header();

// Check if is default container
$is_vc_content = preg_match( "/\[vc_row.*?\]/i", $post->post_content );

// Password protected page doesn't use vc container
if ( post_password_required() ) {
	$is_vc_content = false;
}

// Page title (show or hide)
$show_title = false == $is_vc_content && is_singular() && kalium()->acf->get_field( 'heading_title' );

// Container start
$container = array();

if ( $is_vc_content ) {
	$container[] = 'vc-container';
} else {
	$container[] = 'container';
	$container[] = 'default-margin';
	
	if ( ! is_shop_supported() || ! ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ) {
		$container[] = 'post-formatting';
	}
}
?>
<div class="<?php echo esc_attr( implode( ' ', $container ) ); ?>">
<?php


// Show page title
if ( false == defined( 'HEADING_TITLE_DISPLAYED' ) && apply_filters( 'kalium_page_title', $show_title ) ) {
	?>
	<h1 class="wp-page-title"><?php the_title(); ?></h1>
	<?php
} 

// ---------------------------------------
// Added by Kevin Henry
// ---------------------------------------
?>

<link rel='stylesheet' id='js_composer_front-css'  href='http://www.medicalmotion.de/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.4.5' type='text/css' media='all' />

<style>
	.image {
	background-size: cover;
	max-width: 300px;
	min-height: 150px;
	}

	.image img {
	display: none;
	}

</style>

<div class="container default-margin post-formatting">
<div class="vc-container">
<div class="vc-parent-row row-default"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<h1 style="text-align: center;">Dein Dashboard</h1>

		</div>
	</div>
</div></div></div></div></div><div class="vc-parent-row row-default"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<h2 style="text-align: left;">Deine Ziele</h2>
<p style="text-align: left;">Hier sind die 3 Ziele die du definiert hast :</p>
<?php 
	$priorities = get_user_prio();
	echo '<p><strong>Ziel 1 : '.get_prio_name($priorities['goal_1']).'</strong></p>';
	echo '<p><strong>Ziel 2 : '.get_prio_name($priorities['goal_2']).'</strong></p>';
	echo '<p><strong>Ziel 3 : '.get_prio_name($priorities['goal_3']).'</strong></p>';
?>
		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<h2 style="text-align: left;">Ziele ändern</h2>
<p style="text-align: left;">Du kannst hier den Inhalt von deinem Programm anpassen</p>
		</div>
	</div>

	<div class="wpb_raw_code wpb_content_element wpb_raw_html" >
		<div class="wpb_wrapper">
			<form method="post" action="">
   	<p>
       	<label for="goal_1"><strong>Hauptziel : 4 Tage deiner Woche</strong></label><br />
       	<select name="goal_1" id="goal_1">
       	<?php
       		$programs_list = get_portfolio();
       		for ($i=1; $i<=count($programs_list); $i++)
			{
				echo '<option value="'.$i.'"';
				if ($priorities['goal_1'] == $i)
				{
					echo ' selected ';
				}
				echo '>'.$programs_list[$i].'</option>';
			}
		?>
       	</select><br /><br />
       	<label for="goal_2"><strong>2. Ziel : 2 Tage deiner Woche</strong></label><br />
       	<select name="goal_2" id="goal_2">
       	<?php
       		$programs_list = get_portfolio();
       		for ($i=1; $i<=count($programs_list); $i++)
			{
				echo '<option value="'.$i.'"';
				if ($priorities['goal_2'] == $i)
				{
					echo ' selected ';
				}
				echo '>'.$programs_list[$i].'</option>';
			}
		?>
       	</select><br /><br />
       	<label for="goal_3"><strong>3. Ziel : 1 Tag deiner Woche</strong></label><br />
       	<select name="goal_3" id="goal_3">
        <?php
       		$programs_list = get_portfolio();
       		for ($i=1; $i<=count($programs_list); $i++)
			{
				echo '<option value="'.$i.'"';
				if ($priorities['goal_3'] == $i)
				{
					echo ' selected ';
				}
				echo '>'.$programs_list[$i].'</option>';
			}
		?>
       	</select>
       	<div class="vc_btn3-container vc_btn3-center">
           	<input type="submit" value="Programm ändern" class="vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-primary"/>
       	</div>
</form>
		</div>
	</div>
</div></div></div></div></div>

<div class="vc-parent-row row-default">
	<div class="vc_row wpb_row vc_row-fluid">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner ">
				<div class="wpb_wrapper">
					<div class="wpb_text_column wpb_content_element  post-formatting " >
						<div class="wpb_wrapper">
							<h1 style="text-align: center;">Deine nächsten 8 Tage</h1>
							<p style="text-align: center;">Hier kannst du sehen was für deiner nächsten 8 Tage geplannt ist.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="vc-parent-row row-default">
	<div class="vc_row wpb_row vc_row-fluid">
		<?php
			for ($i=0; $i<=3; $i++)
			{
				echo '		
				<div class="wpb_column vc_column_container vc_col-sm-3">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<p>'.get_date($i).'</p>
								</div>
							</div>
							<div class="wpb_single_image wpb_content_element vc_align_center">
								<figure class="wpb_wrapper vc_figure">
									<div class="vc_single_image-wrapper vc_box_border_grey">
										<div class="image" style="background-image:url('.get_prio_image(get_day_prio($i)).')"><img src="'.get_prio_image(get_day_prio($i)).'" alt="" /></div>
									</div>
								</figure>
							</div>
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<p style="text-align: center;"><strong>'.get_prio_name(get_day_prio($i)).'</strong></p>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}
		?>
	</div>
</div>

<div class="vc-parent-row row-default">
	<div class="vc_row wpb_row vc_row-fluid">
		<?php
			for ($i=4; $i<=7; $i++)
			{
				echo '		
				<div class="wpb_column vc_column_container vc_col-sm-3">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<p>'.get_date($i).'</p>
								</div>
							</div>
							<div class="wpb_single_image wpb_content_element vc_align_center">
								<figure class="wpb_wrapper vc_figure">
									<div class="vc_single_image-wrapper vc_box_border_grey">
										<div class="image" style="background-image:url('.get_prio_image(get_day_prio($i)).')"><img src="'.get_prio_image(get_day_prio($i)).'" alt="" /></div>
									</div>
								</figure>
							</div>
							<div class="wpb_text_column wpb_content_element  post-formatting " >
								<div class="wpb_wrapper">
									<p style="text-align: center;"><strong>'.get_prio_name(get_day_prio($i)).'</strong></p>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}
		?>
	</div>
</div>



</div>


<?php
// ---------------------------------------
// ---------------------------------------
// Page content		
the_content();		

// Container end
?>
</div>
<?php

// Show footer
get_footer();